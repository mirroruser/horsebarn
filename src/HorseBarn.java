import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class HorseBarn {
    private Horse[] spaces;

    public HorseBarn (Horse[] spaces) {
        this.spaces = spaces;
    }

    public int findHorseSpace (String name) {
        for (int i = 0; i < this.spaces.length; i++) {
            if (this.spaces[i] != null && this.spaces[i].getName().equals(name))
                return i;
        }
        return -1;
    }

    public void consolidate () {
        List<Horse> horseList = new ArrayList<Horse>();
        for (Horse h : this.spaces)
            if (h != null) horseList.add(h);

        for (int i = 0; i < this.spaces.length; i++)
            this.spaces[i] = null;

        for (int i = 0; i < horseList.size(); i++)
            this.spaces[i] = horseList.get(i);
    }

    @Override
    public String toString () {
        return "Horsebarn with horses: " +
                Arrays.stream(this.spaces).map(v -> v != null ? v.getName() : "null").collect(Collectors.joining(" "));
    }
}
