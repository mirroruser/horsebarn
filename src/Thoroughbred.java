public class Thoroughbred implements Horse {
    private String name;
    private int weight;

    public Thoroughbred (String name, int weight) {
        this.name = name;
        this.weight = weight;
    }

    @Override
    public String getName () {
        return this.name;
    }

    @Override
    public int getWeight () {
        return this.weight;
    }

    @Override
    public String toString () {
        return "This horse is a Thoroughbred with name " + this.name + " and weight " + this.weight;
    }
}
